dig @localhost -p 65053 ANY aaaa.example.com

# Using the wrong type (A instead of AAAA) results in an erroneous packet

{ "zone": "example.com", "name": "aaaa.example.com", "tld": "com", "sld": "example", "sub": "aaaa"
, "type": "A", "address": "::1" }
