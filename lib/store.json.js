'use strict';

module.exports.create = function (opts) {
  // opts = { filepath };
  var engine = { db: null };

  var db = require(opts.filepath);

  engine.primaryNameservers = db.primaryNameservers;
  engine.getSoas = function (query, cb) {
    var myDomains = db.domains.filter(function (d) {
      return -1 !== query.names.indexOf(d.id.toLowerCase());
    });
    process.nextTick(function () {
      cb(null, myDomains);
    });
  };
  engine.getRecords = function (query, cb) {
    var myRecords = db.records.slice(0).filter(function (r) {

      if ('string' !== typeof r.name) {
        return false;
      }

      // TODO use IN in masterquest (or implement OR)
      // Only return single-level wildcard?
      if (query.name === r.name || ('*.' + query.name.split('.').slice(1).join('.')) === r.name) {
        return true;
      }
    });
    process.nextTick(function () {
      cb(null, myRecords);
    });
  };

  return engine;
};
