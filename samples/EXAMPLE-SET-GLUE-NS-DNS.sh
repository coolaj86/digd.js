#!/bin/bash

node bin/daplie.js --oauth3-config ~/.oauth3+domains@daplie.com ns:set -n hellabit.com --nameserver ns1.daplie.com,ns2.daplie.domains,ns3.hellabit.com



# glue setting is rate-limited, or so it would seem
echo "sleeping between setting glue for rate limit"
sleep 5
node bin/daplie.js --oauth3-config ~/.oauth3+domains@daplie.com glue:set -a 45.55.1.122 -n ns1.daplie.com
sleep 5
node bin/daplie.js --oauth3-config ~/.oauth3+domains@daplie.com glue:set -a 45.55.1.122 -n ns1.daplie.domains
sleep 5
node bin/daplie.js --oauth3-config ~/.oauth3+domains@daplie.com glue:set -a 45.55.1.122 -n ns1.daplie.me --tld me
sleep 5
node bin/daplie.js --oauth3-config ~/.oauth3+domains@daplie.com glue:set -a 45.55.1.122 -n ns1.hellabit.com --tld com
sleep 5

node bin/daplie.js --oauth3-config ~/.oauth3+domains@daplie.com devices:attach -d ns1-do -a 45.55.1.122 -n ns1.daplie.com
node bin/daplie.js --oauth3-config ~/.oauth3+domains@daplie.com devices:attach -d ns1-do -a 45.55.1.122 -n ns1.daplie.domains
node bin/daplie.js --oauth3-config ~/.oauth3+domains@daplie.com devices:attach -d ns1-do -a 45.55.1.122 -n ns1.daplie.me --tld me
node bin/daplie.js --oauth3-config ~/.oauth3+domains@daplie.com devices:attach -d ns1-do -a 45.55.1.122 -n ns1.hellabit.com --tld com



# glue setting is rate-limited, or so it would seem
echo "sleeping between setting glue for rate limit"
sleep 5
node bin/daplie.js --oauth3-config ~/.oauth3+domains@daplie.com glue:set -a 45.55.254.197 -n ns2.daplie.com
sleep 5
node bin/daplie.js --oauth3-config ~/.oauth3+domains@daplie.com glue:set -a 45.55.254.197 -n ns2.daplie.domains
sleep 5
node bin/daplie.js --oauth3-config ~/.oauth3+domains@daplie.com glue:set -a 45.55.254.197 -n ns2.daplie.me --tld me
sleep 5
node bin/daplie.js --oauth3-config ~/.oauth3+domains@daplie.com glue:set -a 45.55.254.197 -n ns2.hellabit.com --tld com
sleep 5

node bin/daplie.js --oauth3-config ~/.oauth3+domains@daplie.com devices:attach -d ns2-do -a 45.55.254.197 -n ns2.daplie.com
node bin/daplie.js --oauth3-config ~/.oauth3+domains@daplie.com devices:attach -d ns2-do -a 45.55.254.197 -n ns2.daplie.domains
node bin/daplie.js --oauth3-config ~/.oauth3+domains@daplie.com devices:attach -d ns2-do -a 45.55.254.197 -n ns2.daplie.me --tld me
node bin/daplie.js --oauth3-config ~/.oauth3+domains@daplie.com devices:attach -d ns2-do -a 45.55.254.197 -n ns2.hellabit.com --tld com



# glue setting is rate-limited, or so it would seem
echo "sleeping between setting glue for rate limit"
sleep 5
node bin/daplie.js --oauth3-config ~/.oauth3+domains@daplie.com glue:set -a 159.203.25.112 -n ns3.daplie.com
sleep 5
node bin/daplie.js --oauth3-config ~/.oauth3+domains@daplie.com glue:set -a 159.203.25.112 -n ns3.daplie.domains
sleep 5
node bin/daplie.js --oauth3-config ~/.oauth3+domains@daplie.com glue:set -a 159.203.25.112 -n ns3.daplie.me --tld me
sleep 5
node bin/daplie.js --oauth3-config ~/.oauth3+domains@daplie.com glue:set -a 159.203.25.112 -n ns3.hellabit.com --tld com
sleep 5

node bin/daplie.js --oauth3-config ~/.oauth3+domains@daplie.com devices:attach -d ns3-do -a 159.203.25.112 -n ns3.daplie.com
node bin/daplie.js --oauth3-config ~/.oauth3+domains@daplie.com devices:attach -d ns3-do -a 159.203.25.112 -n ns3.daplie.domains
node bin/daplie.js --oauth3-config ~/.oauth3+domains@daplie.com devices:attach -d ns3-do -a 159.203.25.112 -n ns3.daplie.me --tld me
node bin/daplie.js --oauth3-config ~/.oauth3+domains@daplie.com devices:attach -d ns3-do -a 159.203.25.112 -n ns3.hellabit.com --tld com
