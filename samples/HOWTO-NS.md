```bash
# Create a glue record for the nameserver
daplie glue:set -n ns1.example.com -a 12.55.12.33

# Set the nameservers for a domain
daplie ns:set -n example.com --nameservers ns1.example.com
```
