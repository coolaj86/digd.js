'use strict';

/*
 * parses files in the format "type|domain|value|json\n"
*/

var filename = process.argv[2];
var fs = require('fs');

var file = fs.readFileSync(filename, 'utf8');
var zone = 'daplie.com';

file.trim().split(/\n/).forEach(function (line) {
  var parts = line.split(/\|/);
  var type = parts[0];
  var name = parts[1] || zone;
  var domain = name.split('.');
  var thing = parts[2];
  var json = JSON.parse(parts[3]);

  var address;                // A, AAAA
  var flag, tag, value;       // CAA
  var data;                   // CNAME, NS, PTR, ... TXT (as array)
  var priority;               // MX, SRV
  var exchange;               // MX
  var weight, port, target;   // SRV

  if (/^(A|AAAA)$/.test(type)) {
    address = thing;
  }
  if (/^(CNAME|NS|PTR)$/.test(type)) {
    data = thing;
  }
  if (/^(TXT)$/.test(type)) {
    data = [ thing ];
  }
  if (/^(MX)$/.test(type)) {
    exchange = thing;
  }
  if (/^(MX|SRV)$/.test(type)) {
    priority = json.priority || 10;
  }

  var obj = {
    zone: zone

  , name: name
  , type: type
  , class: 'IN'
  , ttl: 5 // 12 hours 43200 // 3 days 259200

  , tld: domain.pop()
  , sld: domain.pop()
  , sub: domain.join('.') || undefined

  , address: address
  , aname: undefined
  , flag: flag
  , tag: tag
  , value: value

  , data: data

  , exchange: exchange

  , priority: priority
  , weight: weight
  , port: port
  , target: target
  };

  console.log(",", JSON.stringify(obj));
});
