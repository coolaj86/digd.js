daplie domains:list # shows hellabit.com in my list of domains

# for hellabit.com to lookup itself (chicken and egg problem),
# we must first set glue records
daplie glue:set -n ns1.hellabit.com --tld com -a 138.197.72.1
daplie glue:set -n ns2.hellabit.com --tld com -a 162.243.136.134

# now we can set hellabit.com to use nsx.hellabit.com nameservers
daplie ns:set -n hellabit.com --tld com --nameservers ns1.hellabit.com,ns2.hellabit.com

# now we can't use the dns tools because digd.js does not (yet) have oauth3 compatible apis
# these won't work
# daplie devices:set -d sfo2.devices.hellabit.com -a 138.197.216.176
# daplie devices:attach -d sfo2.devices.hellabit.com -n hellabit.com
# daplie devices:attach -d sfo2.devices.hellabit.com -n www.hellabit.com

# now you can test that your hard work worked
# < ==== NOTE ==== > It may take a few minutes before this starts to work as you'd expect
dig +trace ns1.hellabit.com
dig +trace ns2.hellabit.com

dig +trace hellabit.com
dig +trace www.hellabit.com
